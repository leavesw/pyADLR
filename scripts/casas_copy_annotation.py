"""This script moves annotation from one file to another.
"""

import os
import sys
import logging
import argparse
import progressbar
from dateutil.parser import parse as date_parse
from datetime import datetime


logger = logging.getLogger(os.path.basename(__file__))


def parse_args():
    parser = argparse.ArgumentParser(
        description="Copy annotation from annotated event file to"
                    "another un-annotated event file."
    )
    parser.add_argument('-s', '--src', type=str, required=True,
                        help="The events.csv to copy from.")
    parser.add_argument('-d', '--dst', type=str, required=True,
                        help="The events.csv to copy to.")
    parser.add_argument('--log', type=str, default=None,
                        help='Log directory.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Increase output verbosity.')
    return parser.parse_args()


def check_args(args):
    if not os.path.isfile(args.src):
        logger.error('Cannot find the annotated events %s.' %
                     args.src)
        sys.exit(1)
    if not os.path.isfile(args.dst):
        logger.error('Cannot find the events to be annotated %s.' %
                     args.dst)
        sys.exit(1)


def config_logger(log_path=None, verbose=False):
    """Configure logging options.
    """
    if verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    log_handlers = [logging.StreamHandler()]
    if log_path is not None:
        os.makedirs(log_path, exist_ok=True)
        log_filename = '%s_%s.log' % (
            os.path.basename(__file__),
            datetime.now().strftime('%y%m%dT%H%M%S')
        )
        log_handlers.append(
            logging.FileHandler(
                os.path.join(log_path, log_filename)
            )
        )
    logging.basicConfig(
        level=log_level,
        format='[%(asctime)s] %(name)s:%(levelname)s:%(message)s',
        handlers=log_handlers
    )


def parse_event(line, style='csv_old'):
    tokens = line.strip().split(',')

    if style == 'csv_old':
        time = date_parse('%s %s' % (tokens[0], tokens[1]))
        time_string = time.strftime("%m/%d/%Y %H:%M:%S")
        sensor = tokens[2]
        status = tokens[3]
        resident = tokens[4]
        activity = tokens[5]
        comments = ""
    else:
        time = date_parse(tokens[0])
        time_string = time.strftime("%m/%d/%Y %H:%M:%S")
        sensor = tokens[1]
        status = tokens[2]
        resident = tokens[3]
        activity = tokens[4]
        comments = tokens[5]

    return {
        'time': time,
        'time_string': time_string,
        'sensor': sensor,
        'status': status,
        'resident': resident,
        'activity': activity,
        'comments': comments
    }


def copy_annotation(src, dst, src_style='csv_old', dst_style='csv_new'):
    token_from = []
    token_to = []

    fp_from = open(src, 'r')
    prev_line = ""
    for line in fp_from.readlines():
        if line != prev_line:
            token_from.append(parse_event(line, src_style))
            prev_line = line
    fp_from.close()

    fp_to = open(dst, 'r')
    prev_line = ""
    for line in fp_to.readlines():
        if line != prev_line:
            if line != "\n":
                token_to.append(parse_event(line, dst_style))
            prev_line = line
    fp_to.close()

    total_lines_from = len(token_from)
    total_lines_to = len(token_to)
    logger.info('Loaded %d events from source, from %s to %s.' % (
        total_lines_from, token_from[0]['time_string'],
        token_from[-1]['time_string']
    ))
    logger.info('Loaded %d events from destination, from %s to %s' % (
        total_lines_to, token_to[0]['time_string'],
        token_to[-1]['time_string']
    ))

    with progressbar.ProgressBar(max_value=total_lines_to) as bar:
        src_skip = 0
        dst_skip = 0
        src_i = 0
        dst_i = 0
        while src_i < total_lines_from and dst_i < total_lines_to:
            if token_from[src_i]['time_string'] == token_to[dst_i]['time_string']:
                if token_from[src_i]['sensor'] == token_to[dst_i]['sensor'] and \
                        token_from[src_i]['status'] == token_to[dst_i]['status']:
                    token_to[dst_i]['resident'] = token_from[src_i]['resident']
                    token_to[dst_i]['activity'] = token_from[src_i]['activity']
                    src_i += 1
                    dst_i += 1
                else:
                    if token_from[src_i+1]['time_string'] == \
                            token_to[dst_i]['time_string']:
                        src_i += 1
                        src_skip += 1
                    else:
                        dst_i += 1
                        dst_skip += 1
                    logger.debug('Error: sensor name or status mismatch '
                                 '(src %d, dst %d).' % (src_i, dst_i))
            else:
                if date_parse(token_from[src_i]['time_string']) < \
                        date_parse(token_to[dst_i]['time_string']):
                    src_i += 1
                    src_skip += 1
                else:
                    token_to[dst_i]['resident'] = token_from[src_i]['resident']
                    token_to[dst_i]['activity'] = token_from[src_i]['activity']
                    dst_i += 1
                    dst_skip += 1
            bar.update(dst_i)
        if src_i < total_lines_from:
            src_skip += total_lines_from - src_i
        if dst_i < total_lines_to:
            dst_skip += total_lines_to - dst_i

    logger.info('Annotations of %d events are copied.' %
                (total_lines_to - dst_skip))
    logger.info('Skipped %d events in destination; %d events in source.' % (
        dst_skip, src_skip
    ))

    return token_to


def write_back(dst, tokens):
    fp = open(dst, 'w+')
    with progressbar.ProgressBar(max_value=len(tokens)) as bar:
        for i, event in enumerate(tokens):
            time_string = event['time'].strftime("%m/%d/%Y %H:%M:%S.%f ") + \
                            event['time'].strftime("%z")[:3] + ":" + \
                            event['time'].strftime("%z")[3:]
            string_list = [time_string, event['sensor'], event['status'],
                           event['resident'], event['activity'],
                           event['comments']]
            fp.write(','.join(string_list) + '\n')
            bar.update(i)
    fp.close()


if __name__ == '__main__':
    args = parse_args()
    check_args(args)
    config_logger(log_path=args.log, verbose=args.verbose)
    tokens = copy_annotation(src=args.src, dst=args.dst)
    write_back(dst=args.dst, tokens=tokens)
