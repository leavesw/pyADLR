import os
import sys
import progressbar
import argparse
import logging
from datetime import datetime
from pyADLR.casas import CASASDataset


logger = logging.getLogger(__name__)
default_log_dir = os.path.join(
    os.path.dirname(__file__),
    'log'
)
progressbar.streams.wrap_stderr()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str, default='',
                        help='Path to CASAS dataset directory.')
    parser.add_argument('--site', type=str, default='',
                        help='Path to associated CASAS site metadata directory.')
    parser.add_argument('--log', type=str, default=default_log_dir,
                        help='Directory to store logs.')
    parser.add_argument('--output', type=str, default=None,
                        help='Output filename.')
    parser.add_argument('--start', type=int, default=0,
                        help='Start index when the features are exported in xlsx format.')
    parser.add_argument('--end', type=int, default=-1,
                        help='Stop index when the features are exported in xlsx format.')
    parser.add_argument('--verbose', type=bool, default=True,
                        help='Increase log verbosity.')
    return parser.parse_args()


def check_args(args):
    """Check parser args and make sure that it fits the requirement.
    """
    if not os.path.isdir(args.dataset):
        logger.error('Dataset %s not found.' % args.dataset)
        sys.exit(1)
    if not os.path.isdir(args.site):
        logger.error('Smart home site %s not found.' % args.site)
        sys.exit(1)
    if os.path.isfile(args.output):
        overwrite = input('File %s already exists. Do you want to overwrite it (y/n)?')
        if overwrite != 'y':
            sys.exit(0)


def logging_config(log_path, verbose=False):
    """Configure logging options.
    """
    os.makedirs(log_path, exist_ok=True)
    if verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    log_filename = '%s_%s.log' % (
        os.path.splitext(os.path.basename(__file__))[0],
        datetime.now().strftime('%y%m%dT%H%M%S')
    )
    logging.basicConfig(
        level=log_level,
        format='[%(asctime)s] %(name)s:%(levelname)s:%(message)s',
        handlers=[logging.StreamHandler(),
                  logging.FileHandler(
                      os.path.join(log_path, log_filename)
                  )]
    )


def load_dataset(dataset_path, site_path, output_path):
    """Load CASAS smart home dataset from folder"""
    dataset = CASASDataset(directory=dataset_path, site_dir=site_path)
    dataset_chkpt_file = os.path.join(output_path,
                                      dataset.data_dict['name'] + ".pkl")
    if os.path.exists(dataset_chkpt_file):
        dataset = CASASDataset.load(dataset_chkpt_file)
    else:
        dataset.enable_sensor_with_sensor_categories([
            'Motion', 'Item', 'Door'
        ])
        dataset.load_events(show_progress=True)
        dataset.save(dataset_chkpt_file)
    dataset.summary()
    return dataset


if __name__ == "__main__":
    args = parse_args()
    check_args(args)
    logging_config(log_path=args.log, verbose=args.verbose)
    dataset = load_dataset(dataset_path=args.dataset,
                           site_path=args.site,
                           output_path=args.log)
    ext = os.path.splitext(args.output)[1]
    if ext == '.xlsx':
        dataset.export_xlsx(
            filename=args.output,
            comments='',
            mode='raw',
            normalized=True,
            sensor_one_hot=True,
            start=args.start,
            end=args.end
        )
    elif ext == '.hdf5':
        dataset.export_hdf5(
            filename=args.output,
            comments='',
            mode='raw',
            normalized=True,
            sensor_one_hot=True
        )
    else:
        logger.error('Output file has to be .hdf5 or .xlsx.')
        exit(1)
