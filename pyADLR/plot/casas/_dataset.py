from mayavi import mlab
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from ...casas import CASASDataset


def dataset_plot_sensor_distance(dataset, distance, sensor, title=""):
    """Plot sensor distance on floorplan

    Args:
        dataset (:obj:`~pyADLR.casas.CASASDataset`): CASAS smart home dataset.
        distance (:obj:`numpy.ndarray`): Distance array matrix of shape
            (num_sensor, num_sensor).
        sensor (:obj:`int` or :obj:`string`): The ID or name of the sensor of
            interest.
        title (:obj:`str`): Title of the plot.
    """
    assert(isinstance(dataset, CASASDataset))
    if sensor is str:
        sensor_id = dataset.sensor_indices_dict[sensor]
    else:
        sensor_id = sensor
    drawing_data = dataset.site.prepare_floorplan()
    # Prepare aux data structure
    sensor_list = set(sensor['name'] for sensor in dataset.sensor_list)
    fig, (ax) = plt.subplots(1, 1)
    fig.set_size_inches(18, 18)
    ax.imshow(drawing_data['img'])
    active_patch_list = []
    # Draw Sensor block patches
    for key, patch in drawing_data['sensor_boxes'].items():
        if key in sensor_list:
            ax.add_patch(patch)
            active_patch_list.append(patch)
    # Draw Sensor name
    for key, text_data in drawing_data['sensor_texts'].items():
        if key in sensor_list:
            text_x, text_y, text = text_data
            temp_distance = distance[
                sensor_id, dataset.sensor_indices_dict[key]
            ]
            text += "\n%.4f" % temp_distance
            temp_color = 'red' if temp_distance == 0 else 'black'
            ax.text(text_x, text_y, text, color=temp_color,
                    backgroundcolor=mcolors.colorConverter.to_rgba('#D3D3D3', 0.7),
                    horizontalalignment='center', verticalalignment='top',
                    zorder=3)
    plt.title("%s - %s %s" % (
        dataset.data_dict['name'], dataset.sensor_list[sensor_id]['name'],
        title))
    plt.show()


def dataset_plot_sensor_embedding(dataset, embeddings, figure=None):
    """Plot sensor embedding in 3D space using mayavi.

    Given the dataset and a sensor embedding matrix, each sensor is shown as
    a sphere in the 3D space. Note that the shape of embedding matrix is
    (num_sensors, 3) where num_sensors corresponds to the length of
    ``dataset.sensor_list``. All embedding vectors range between 0 and 1.

    Args:
        dataset (:obj:`~pymrt.casas.CASASDataset`): CASAS smart home dataset.
        embeddings (:obj:`numpy.ndarray`): 3D sensor vector embedding.
    """
    show_figure = False
    if figure is None:
        show_figure = True
        figure = mlab.figure('Sensor Embedding (3D)')
    # Plot sensors, texts and outlines
    figure.scene.disable_render = True
    points = mlab.points3d(embeddings[:, 0], embeddings[:, 1], embeddings[:, 2],
                           scale_factor=0.015)
    for i, x in enumerate(embeddings):
        mlab.text3d(x[0], x[1], x[2], dataset.sensor_list[i]['name'],
                    scale=(0.01, 0.01, 0.01))
    mlab.outline(None, color=(.7, .7, .7), extent=[0, 1, 0, 1, 0, 1])
    ax = mlab.axes(None, color=(.7, .7, .7), extent=[0, 1, 0, 1, 0, 1],
                   ranges=[0, 1, 0, 1, 0, 1], nb_labels=6)
    ax.label_text_property.font_size = 3
    ax.axes.font_factor = 0.3
    figure.scene.disable_render = False
    if show_figure:
        mlab.show()
    return figure, points
