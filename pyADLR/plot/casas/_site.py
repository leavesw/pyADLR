import os
import matplotlib.image as mimg
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from ...casas import CASASSite
from ...casas import CASASSensorType


def site_prepare_plot(site, categories=None):
    """Prepare the floorplan for drawing

    This internal function generates a dictionary of elements to be ploted
    with matplotlib.
    The return is a dictionary composed of following keys:

    * ``img``: floor plan image loaded in :obj:`mimg` class for plotting
    * ``width``: actual width of the image
    * ``height``: actual height of the image
    * ``sensor_centers``: list of the centers to plot each sensor text.
    * ``sensor_boxes``: list of rectangles to show the location of sensor
      (:obj:`patches.Rectangle`).
    * ``sensor_texts``: list of sensor names

    Returns:
        :obj:`dict`: A dictionary contains all the pieces needed to draw
            the floorplan
    """
    assert(isinstance(site, CASASSite))
    floorplan_dict = {}
    img = mimg.imread(os.path.join(
        site.directory.replace('\\', '/'), site.data_dict['floorplan'])
    )
    img_x = img.shape[1]
    img_y = img.shape[0]
    # Create Sensor List/Patches
    sensor_boxes = {}
    sensor_texts = {}
    sensor_centers = {}
    # Check Bias
    for sensor in site.data_dict['sensors']:
        loc_x = sensor['locX'] * img_x
        loc_y = sensor['locY'] * img_y
        width_x = 0.01 * img_x
        width_y = 0.01 * img_y
        sensor_category = \
            CASASSensorType.get_best_category_for_sensor(sensor['types'])
        if categories is not None and sensor_category not in categories:
            continue
        sensor_color = CASASSensorType.get_category_color(sensor_category)
        sensor_boxes[sensor['name']] = \
            mpatches.Rectangle((loc_x, loc_y),
                               width_x, width_y,
                               edgecolor='grey',
                               facecolor=sensor_color,
                               linewidth=1,
                               zorder=2)
        sensor_texts[sensor['name']] = (loc_x,
                                        loc_y,
                                        sensor['name'])
        sensor_centers[sensor['name']] = (loc_x, loc_y)
    # Populate dictionary
    floorplan_dict['img'] = img
    floorplan_dict['width'] = img_x
    floorplan_dict['height'] = img_y
    floorplan_dict['sensor_centers'] = sensor_centers
    floorplan_dict['sensor_boxes'] = sensor_boxes
    floorplan_dict['sensor_texts'] = sensor_texts
    return floorplan_dict


def site_plot(floorplan_dict, filename=None, fontsize=10):
    fig, (ax) = plt.subplots(1, 1, figsize=(18, 18))
    ax.imshow(floorplan_dict['img'], zorder=1)
    # Draw Sensor block patches
    for key, patch in floorplan_dict['sensor_boxes'].items():
        ax.add_patch(patch)
    # patch_collection = mcollections.PatchCollection(
    #     floorplan_dict['sensor_boxes'].values(), zorder=2
    # )
    # ax.add_collection(patch_collection)
    ax.set_xlim(0, floorplan_dict['width'])
    ax.set_ylim(0, floorplan_dict['height'])
    # Draw Sensor name
    plot_texts = []
    for key, text in floorplan_dict['sensor_texts'].items():
        plot_texts.append(ax.text(
            *text, color='black',
            #backgroundcolor=mcolors.colorConverter.to_rgba('#D3D3D3', 0.2),
            horizontalalignment='center', verticalalignment='top',
            zorder=3, fontsize=fontsize
        ))
    # adjustText.adjust_text(
    #     plot_texts, only_move={'text': 'y'},
    #     #arrowprops=dict(arrowstyle="->", color='k', lw=0.5)
    # )
    if floorplan_dict.get('sensor_lines', None) is not None:
        for key, line in floorplan_dict['sensor_lines'].items():
            ax.add_line(line)
    fig.tight_layout()
    if filename is None:
        # Show image
        fig.show()
    else:
        fig.savefig(filename)
