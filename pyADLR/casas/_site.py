import os
import json
import logging


logger = logging.getLogger(__name__)


class CASASSite:
    """CASAS Site Class

    This class parses the metadata for a smart home site and loaded it into
    prameter ``data_dict``.
    Usually, the description of a site is composed of following keys:

    * ``name``: Site (Testbed) name.
    * ``floorplan``: Relative path to the floorplan image (usually \*.png).
    * ``sensors``: List of sensors populated in the smarthome.
    * ``timezone``: Local timezone of the site (IANA string).

    Each sensor in the metadata is a dictionary composed of following keys:

    * ``name``: Target name of the sensor.
    * ``types``: List of sensor types.
    * ``locX``: x position relative to the width of floorplan (between 0 and 1)
    * ``locY``: y position relative to the height of floorplan (between 0 and 1)
    * ``sizeX``: depricated.
    * ``sizeY``: depricated.
    * ``description``: custom description added to the sensor.
    * ``tag``: tag string of the sensor.
    * ``serial``: list of serial number for the tag.

    Note that the name and types are the two that matter in multi-resident
    activity recognition implementation.

    Attributes:
        data_dict (:obj:`dict`): A dictionary contains information about smart
            home.
        directory (:obj:`str`): Directory that stores CASAS smart home site data

    Parameters:
        directory (:obj:`str`): Directory where the meta-data describing a
            CASAS smart home site is stored.
    """
    def __init__(self, directory):
        if os.path.isdir(directory):
            self.directory = directory
            site_json_fname = os.path.join(directory, 'site.json')
            if os.path.exists(site_json_fname):
                f = open(site_json_fname, 'r')
                self.data_dict = json.load(f)
                self.sensor_list = self.data_dict['sensors']
                # Populate sensor_dict (dict type) for faster sensor
                # information lookup.
                self.sensor_dict = self._populate_sensor_lookup_dict()
            else:
                logger.error('Smart home metadata file %s does not exist. '
                             'Create an empty CASASHome Structure'
                             % site_json_fname)
                raise FileNotFoundError('File %s not found.' % site_json_fname)

    def _populate_sensor_lookup_dict(self):
        """ Populate a dictionary structure to find each sensor by target name.

        Returns:
            :obj:`dict`: A dictionary indexed by sensor target name (:obj:`str`)
                with a dictionary structure detailing the information of the
                sensor (:obj:`dict`).
        """
        sensor_dict = {}
        for sensor in self.sensor_list:
            sensor_dict[sensor['name']] = sensor
        return sensor_dict

    def get_name(self):
        """Get the smart home name

        Returns:
            :obj:`str`: smart home name
        """
        return self.data_dict['name']

    def get_sensor(self, name):
        """Get the information about the sensor

        Parameters:
            name (:obj:`str`): name of the sensor

        Returns:
            :obj:`dict`: A dictionary that stores sensor information
        """
        for sensor in self.data_dict['sensors']:
            if sensor['name'] == name:
                return sensor
        return None

    def get_all_sensor_names(self):
        """Get All Sensor Names

        Returns:
            :obj:`list` of :obj:`str`: a list of sensor names.
        """
        names = [sensor['name'] for sensor in self.sensor_list]
        return names

    def get_all_sensor_types(self):
        """ Get All Sensor Types

        Returns
            :obj:`dict`: A dictionary indexed by sensor type (:obj:`str`)
                with the value of a list of sensor target names that belongs
                to the index type (:obj:`list` of :obj:`str`).
        """
        sensor_types_dict = {}
        for sensor in self.sensor_list:
            for sensor_type in sensor['types']:
                if sensor_type not in sensor_types_dict:
                    sensor_types_dict[sensor_type] = [sensor['name']]
                elif sensor['name'] not in sensor_types_dict[sensor_type]:
                    sensor_types_dict[sensor_type].append(sensor['name'])
        return sensor_types_dict

    def sensor_type_summary(self):
        """Print summary on all types of sensors on site
        """
        sensor_types_dict = self.get_all_sensor_types()
        print('%d types of sensors on site' % len(sensor_types_dict))
        for sensor_type, sensor_list in sensor_types_dict.items():
            print('    %s:\n        %s' %
                  (sensor_type, ",\n        ".join(sensor_list)))

    def summary(self):
        """Print brief site summary
        """
        print('[%s]: %s' % (self.get_name(), self.directory))
        print('\t sensors %d' % len(self.sensor_list))
        print('\t floorplan %s\n' % os.path.join(
            self.directory, self.data_dict['floorplan']
        ))

    @staticmethod
    def load_all_sites(sites_dir=None):
        """Load all CASAS sites from data folder

        Returns:
            :obj:`dict` of :obj:`CASASSite`: indexed by the name (:obj:`str`)
                of each site.
        """
        if sites_dir is None:
            from . import SITES_DIR
            sites_dir = SITES_DIR
        sites_list = {}
        if os.path.isdir(sites_dir):
            site_names = os.listdir(sites_dir)
            for site_name in site_names:
                site_dirname = os.path.join(sites_dir, site_name)
                if os.path.isdir(site_dirname):
                    cur_site = CASASSite(site_dirname)
                    sites_list[cur_site.get_name()] = cur_site
        return sites_list

