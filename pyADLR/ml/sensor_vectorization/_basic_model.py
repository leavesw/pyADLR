import math
import time
import logging
import tensorflow as tf
from datetime import datetime
from .._tf_helper import variable_on_cpu
from .._tf_helper import add_loss_summaries
from ._skip_gram import SkipGramGenerator

logger = logging.getLogger(__name__)
FLAGS = tf.app.flags.FLAGS


class SvBasicModel:
    """Sensor vectorization basic model.
    """
    def __init__(self, num_sensors, embedding_size, random_seed=0.):
        self.num_sensors = num_sensors
        self.embedding_size = embedding_size

        # Initialize variable
        with tf.variable_scope('sensor_vector', reuse=tf.AUTO_REUSE) as scope:
            self.embeddings = variable_on_cpu(
                'embedding_matrix',
                shape=[self.num_sensors, self.embedding_size],
                initializer=tf.random_uniform_initializer(
                    minval=-1.,
                    maxval=1.,
                    seed=random_seed
                )
            )

        with tf.variable_scope('nce_layer', reuse=tf.AUTO_REUSE) as scope:
            self.nce_weights = variable_on_cpu(
                'weights',
                shape=[self.num_sensors, self.embedding_size],
                initializer=tf.truncated_normal_initializer(
                    mean=0.,
                    stddev=1./math.sqrt(self.embedding_size)
                )
            )
            self.nce_biases = variable_on_cpu(
                'biases',
                shape=[self.num_sensors],
                initializer=tf.constant_initializer(value=0.)
            )

    def inference(self, input_x):
        """Logits of mostly likely adjacent sensor to a specific input.

        Args:
            input_x: Tensor of shape `[batch_size, 1]` of type tf.int64.
        """
        embeds = self.embeds(input_x=input_x)
        logits = tf.nn.bias_add(
            tf.matmul(embeds, tf.transpose(self.nce_weights)),
            self.nce_biases
        )
        return logits

    def nce_loss(self, embed_x, label_y, num_samples=1,
                 num_neg_samples=None):
        if num_neg_samples is None:
            num_neg_samples = self.num_sensors

        loss = tf.reduce_mean(
            tf.nn.nce_loss(
                weights=self.nce_weights,
                biases=self.nce_biases,
                labels=label_y,
                inputs=embed_x,
                num_sampled=num_neg_samples,
                num_classes=self.num_sensors,
                num_true=num_samples
            )
        )

        return loss

    def embeds(self, input_x):
        return tf.nn.embedding_lookup(self.embeddings, input_x)

    def softmax_loss(self, logits, label_y):
        labels_one_hot = tf.one_hot(label_y, self.num_sensors)
        loss = tf.reduce_sum(
            tf.nn.sigmoid_cross_entropy_with_logits(
                labels=labels_one_hot,
                logits=logits
            )
        )
        return loss

    def train(self, total_loss, global_step,
              epoch_size, num_epochs_per_decay,
              init_learning_rate, learning_rate_decay_factor,
              moving_average_decay):
        """Train ShlConvNN model

        Returns:
             train_op: The training operator
        """
        # Variable that affect learning rate.
        num_batches_per_epoch = epoch_size / FLAGS.batch_size
        decay_steps = int(num_batches_per_epoch * num_epochs_per_decay)

        # Decay the learning rate exponentially based on the number of steps
        lr = tf.train.exponential_decay(
            init_learning_rate,
            global_step,
            decay_steps,
            learning_rate_decay_factor,
            staircase=True
        )
        tf.summary.scalar('learning_rate', lr)

        # Generate moving average of loss
        loss_averages_op = add_loss_summaries(total_loss=total_loss)

        # Compute Gradients
        with tf.control_dependencies([loss_averages_op]):
            opt = tf.train.GradientDescentOptimizer(learning_rate=lr)
            grads = opt.compute_gradients(total_loss)

        # Apply gradients
        apply_gradient_op = opt.apply_gradients(grads, global_step=global_step)

        # Add histograms for trainable variables
        for var in tf.trainable_variables():
            tf.summary.histogram(var.op.name, var)

        # Add histograms for gradients
        for grad, var in grads:
            if grad is not None:
                tf.summary.histogram(var.op.name + '/gradients', grad)

        # Track the moving averages of all trainable variables.
        variable_averages = tf.train.ExponentialMovingAverage(
            decay=moving_average_decay,
            num_updates=global_step
        )

        with tf.control_dependencies([apply_gradient_op]):
            variables_averages_op = variable_averages.apply(
                tf.trainable_variables())

        return variables_averages_op


def s2v_basic_train(num_sensors,
                    sensor_sequence,
                    embedding_size=3,
                    batch_size=128,
                    skip_window=5,
                    num_samples=4,
                    num_neg_samples=None,
                    num_steps=100001,
                    init_learning_rate=1.0,
                    num_epochs_per_decay=200,
                    learning_rate_decay_factor=0.1,
                    moving_average_decay=0.9999,
                    random_seed=0.):
    training_dataset = tf.data.Dataset.from_generator(
        generator=SkipGramGenerator(sequence=sensor_sequence,
                                    skip_window=skip_window,
                                    num_samples=num_samples,
                                    recursive=True),
        output_types=(tf.int32, tf.int32),
        output_shapes=(tf.TensorShape(dims=[]),
                       tf.TensorShape(dims=[num_samples]))
    )
    batched_training_dataset = training_dataset.batch(FLAGS.batch_size)
    graph = tf.Graph()
    with graph.as_default():
        global_step = tf.train.get_or_create_global_step()
        with tf.device('/cpu:0'):
            data_x, label_y = \
                batched_training_dataset.make_one_shot_iterator().get_next()
        model = SvBasicModel(num_sensors=num_sensors,
                             embedding_size=embedding_size,
                             random_seed=random_seed)
        loss = model.nce_loss(
            embed_x=model.embeds(data_x),
            label_y=label_y,
            num_neg_samples=num_neg_samples,
            num_samples=num_samples
        )
        train_op = model.train(
            total_loss=loss,
            global_step=global_step,
            epoch_size=sensor_sequence.shape[0],
            num_epochs_per_decay=num_epochs_per_decay,
            init_learning_rate=init_learning_rate,
            learning_rate_decay_factor=learning_rate_decay_factor,
            moving_average_decay=moving_average_decay
        )

        class _LoggerHook(tf.train.SessionRunHook):
            """Logs loss and runtime."""

            def begin(self):
                self._step = -1
                self._start_time = time.time()

            def before_run(self, run_context):
                self._step += 1
                return tf.train.SessionRunArgs(loss)  # Asks for loss value.

            def after_run(self, run_context, run_values):
                if self._step % FLAGS.log_frequency == 0:
                    current_time = time.time()
                    duration = current_time - self._start_time
                    self._start_time = current_time

                    loss_value = run_values.results
                    examples_per_sec = FLAGS.log_frequency * FLAGS.batch_size / duration
                    sec_per_batch = float(duration / FLAGS.log_frequency)

                    format_str = (
                        '%s: step %d, loss = %.2f (%.1f examples/sec; %.3f '
                        'sec/batch)')
                    print(format_str % (datetime.now(), self._step, loss_value,
                                        examples_per_sec, sec_per_batch))

        with tf.train.MonitoredTrainingSession(
                checkpoint_dir=FLAGS.train_dir,
                hooks=[tf.train.StopAtStepHook(last_step=FLAGS.max_steps),
                       tf.train.NanTensorHook(loss),
                       _LoggerHook()],
                config=tf.ConfigProto(
                    log_device_placement=FLAGS.log_device_placement
                )) as mon_sess:
            while not mon_sess.should_stop():
                mon_sess.run(train_op)


def s2v_basic_load(num_sensors, embedding_size,
                   checkpoint_dir, latest_filename=None,
                   moving_average_decay=0.99):
    graph = tf.Graph()
    with graph.as_default():
        model = SvBasicModel(num_sensors=num_sensors,
                             embedding_size=embedding_size)
        with tf.Session() as sess:
            ckpt = tf.train.get_checkpoint_state(
                checkpoint_dir=checkpoint_dir,
                latest_filename=latest_filename
            )
            if ckpt and ckpt.model_checkpoint_path:
                variable_averages = tf.train.ExponentialMovingAverage(
                    decay=moving_average_decay
                )
                variables_to_restore = variable_averages.variables_to_restore()
                saver = tf.train.Saver(variables_to_restore)
                saver.restore(sess, ckpt.model_checkpoint_path)
                global_step = \
                    ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                logger.info(global_step)
                norm = tf.sqrt(
                    tf.reduce_sum(tf.square(model.embeddings), 1,
                                  keep_dims=True)
                )
                normalized_embeddings = model.embeddings / norm
                sensor_embeddings = normalized_embeddings.eval(session=sess)

                return sensor_embeddings
            else:
                logger.error('Cannot find the checkpoint file specified.')
                raise ValueError
