import logging
import numpy as np


logger = logging.getLogger(__name__)


class SkipGramGenerator:
    """Sensor vectorization based on a skip-gram model.

    Mapping sensors into an `m`-dimensional space where the distance between
    two sensors represents their spatio-temporal adjacency in smart home
    dataset.
    """
    def __init__(self, sequence, skip_window, num_samples,
                 filtered=True, recursive=True):
        if num_samples > skip_window:
            logger.error('Number of samples per window should be smaller'
                         'than the width of window.')
            exit(1)
        self.sequence = sequence
        self.skip_window = skip_window
        self.num_samples = num_samples
        self.filtered = filtered
        self.recursive = recursive
        # Get total length of sequence
        self.sequence_length = sequence.shape[0]
        # Temporary value
        self.cur_id = 0
        self.window_id = 1

    def __call__(self):
        while True:
            self.cur_id += 1
            if self.cur_id >= self.sequence_length - self.skip_window:
                self.cur_id = 0
            random_indices = np.random.randint(low=1, high=self.skip_window,
                                               size=self.num_samples)
            yield self.sequence[self.cur_id], \
                self.sequence[random_indices + self.cur_id]
